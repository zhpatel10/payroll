package payroll;

public class Employee {
    protected String name;
    protected double hourlyWage;
    protected int hours;

    public Employee(String name, double hourlyWage, int hours) {
        this.name = name;
        this.hourlyWage = hourlyWage;
        this.hours = hours;
    }

    public String getName() {
        return name;
    }

    public double getHourlyWage() {
        return hourlyWage;
    }

    public int getHours() {
        return hours;
    }

    public double calculatePay()
    {
        double pay= this.hours * this.hourlyWage;
        return pay;
    }
}
