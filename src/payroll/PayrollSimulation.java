package payroll;

public class PayrollSimulation {
    public static void main(String[] args) {
        Employee emp=new Employee("Zalak",20,7);
        System.out.println("Emplpoyee's paycheque: " + emp.calculatePay());

        Manager man=new Manager("Neha",27,9);
        man.setBonus(25);
        System.out.println("Manager's paycheque: " + man.calculatePay());
    }
}
